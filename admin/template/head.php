<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>ระบบสารสนเทศฝึกประสบการณ์วิชาชีพ</title>

    <link rel="stylesheet" type="text/css" href="css/bootstrap.min" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />
    <script type="text/javascript" src="js/jquery-1.7.2.min"></script>  
 
    <script type="text/javascript" src="js/bootstrap.min"></script>   
     <script type='text/javascript' src='js/jquery.nivo.slider.js'></script> 
        <script type='text/javascript' src='js/jquery.validate.js'></script> 

    <script language="javascript">
$(window).load(function() {
           
                    $('#slider').nivoSlider({
                        effect:'random', //Specify sets like: 'fold,fade,sliceDown'
                        slices:15,
                        animSpeed:500, //Slide transition speed
                        pauseTime:3000,
                        startSlide:0, //Set starting Slide (0 index)
                        directionNav:false, //Next & Prev
                        directionNavHide:true, //Only show on hover
                        controlNav:false, //1,2,3...
                        controlNavThumbs:true, //Use thumbnails for Control Nav
                        controlNavThumbsFromRel:false, //Use image rel for thumbs
                        controlNavThumbsSearch: '.jpg', //Replace this with...
                        controlNavThumbsReplace: '_thumb.jpg', //...this in thumb Image src
                        keyboardNav:true, //Use left & right arrows
                        pauseOnHover:false, //Stop animation while hovering
                        manualAdvance:false, //Force manual transitions
                        captionOpacity:0.8, //Universal caption opacity
                        beforeChange: function(){},
                        afterChange: function(){},
                        slideshowEnd: function(){} //Triggers after all slides have been shown
                    });
            
            });
            
       

    </script>
</head>

<?php
include('config/config.php');

?>