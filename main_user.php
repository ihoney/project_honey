<!DOCTYPE HTML>
<html>
    <?php include('template/head.php') ?>

    <body>
        <div id="wrapper">
            <div class="top-wrapper">
                <?php include('template/header2.php') ?>
            </div>
            <div class="user-content">
                <div class="row-fluid">

                    <div class="span3">
                        <div class="side-profile">

                            <div class="user-img">
                                <img src="img/honey.jpg" class="img-polaroid">
                            </div>
                            <div class="user-profile">
                                <div class="row-fluid">
                                    <div class="span3"><p class="user-profile-text">ชื่อ</p></div>
                                    <div class="span9"><p class="user-profile-text2">ฮันนี่</p></div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span3"><p class="user-profile-text">อายุ</p></div>
                                    <div class="span9"><p class="user-profile-text2">25</p></div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span3"><p class="user-profile-text">ที่อยู่</p></div>
                                    <div class="span9"><p class="user-profile-text2">บ้านกุดนางทุย</p></div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span4"><p class="user-profile-text">เบอร์โทร</p></div>
                                    <div class="span8"><p class="user-profile-text2">0809999999</p></div>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="span9">
                        <div class="user-tool">
                            <div class="tabbable"> <!-- Only required for left/right tabs -->
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab1" data-toggle="tab">ประวัติส่วนตัว</a></li>
                                    <li><a href="#tab2" data-toggle="tab">ส่งคำร้องขอฝึกประสบการณ์</a></li>
                                    <li><a href="#tab3" data-toggle="tab">ผลอนุมัติการฝึกประสบการณ์</a></li>
                                    <li><a href="#tab4" data-toggle="tab">สถานะการส่งเอกสาร</a></li>
                                    <li><a href="#tab5" data-toggle="tab">บันทึกประจำวัน</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab1">
                                        
                                         <p>ประวัติ</p>
                                    </div>
                                    <div class="tab-pane" id="tab2">
                                        <p>ฟอร์มส่งคำร้องขอฝึกประสบการณ์</p>
                                    </div>
                                    <div class="tab-pane" id="tab3">
                                        <p>ผลอนุมัติ</p>
                                    </div>
                                    <div class="tab-pane" id="tab3">
                                        <p>สถานะการส่งเอกสาร</p>
                                    </div>
                                    <div class="tab-pane" id="tab3">
                                        <p>บันทึกประจำวัน</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="bottom-wrapper">
                <?php include('template/footer.php') ?>
            </div>
        </div>

    </body>
</html>