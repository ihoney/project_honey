<div class="header">
    <div class="header-navigation">
        <ul >
            <!--  <li><a href="#">หน้าหลัก</a></li>
             <li><a href="#">เว็บบอร์ด</a></li>-->
            <li><a href="#login" data-toggle="modal">เข้าสู่ระบบ</a></li>   
            <li><a href="#signup" data-toggle="modal">สมัครสมาชิก</a></li>   
        </ul>
    </div>
    <div id="login" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Login</h3>
        </div>
        <div class="modal-body">

            <div class="user-login">
                <form  method="post" action="index.php">
                    <div style="margin: 0 auto;width: 80%;padding: 20px 0;">
                        <div class="row-fluid">
                            <div class="span4">Username :</div>
                            <div class="span8"><input type="text" name="u_username"></div>
                        </div>
                        <div class="row-fluid">
                            <div class="span4">Password :</div>
                            <div class="span8"><input type="password" name="u_pass"></div>
                        </div>
                    </div>

            </div>

        </div>
        <div class="modal-footer" >
            <div style="width: 30%;margin: 0 auto;">
                <input type="submit" class="btn btn-primary" name="submit-login" value="ล๊อกอิน">

                <button class="btn" data-dismiss="modal" aria-hidden="true">ยกเลิก</button>     
            </div>
            </form>
        </div>
    </div>

    <div id="signup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Signup</h3>
        </div>
        <div class="modal-body">
            <form  method="post" action="index.php" id="form-subit">
                <div class="user-login">
                    <div style="margin: 0 auto;width: 90%;padding: 10px 0;">
                        <div class="row-fluid">
                            <div class="span4">Username :</div>
                            <div class="span7 offset1"><input type="text" name="u_username"  class="required" minlength="4" maxlength="12" ></div>
                        </div>
                        <div class="row-fluid">
                            <div class="span4">Password :</div>
                            <div class="span7 offset1"><input type="password" name="u_pass" id="password_again"  class="required" minlength="6" maxlength="12" ></div>
                        </div>
                        <div class="row-fluid">
                            <div class="span5">Confirm Password :</div>
                            <div class="span6"><input type="password" name="c_pass" class="required" equalTo="#password_again" minlength="6" maxlength="12"></div>
                        </div>
                        <div class="row-fluid">
                            <div class="span4">Email :</div>
                            <div class="span7 offset1"><input type="text" name="u_email"  class="required email"></div>
                        </div>
                        <hr style="padding: 10px 0;margin-top: 10px;">
                        <div class="row-fluid">
                            <div class="span4">ชื่อ-สกุล :</div>
                            <div class="span7 offset1"><input type="text" name="u_name" class="required" maxlength="100"></div>
                        </div>
                        <div class="row-fluid">
                            <div class="span4">วันเกิด :</div>
                            <div class="span7 offset1"><input type="text"  name="u_birthday" id="date"  /></div>
                        </div>
                        <div class="row-fluid">
                            <div class="span4">ที่อยู่ :</div>
                            <div class="span7 offset1"><textarea name="u_address"></textarea></div>
                        </div>
                        <div class="row-fluid">
                            <div class="span4">เบอร์โทร :</div>
                            <div class="span7 offset1"><input type="text" name="u_tel" class="number"></div>
                        </div>
                    </div>
                </div>

        </div>
        <div class="modal-footer" >
            <div style="width: 40%;margin: 0 auto;">
                <input type="submit" class="btn btn-primary" name="submit-signup" value="สมัครสมาชิก">
                <button class="btn" data-dismiss="modal" aria-hidden="true">ยกเลิก</button>     
            </div>
            </form>
        </div>
    </div>
    <div class="main-header">
        <div class="row-fluid">
            <div class="span2">
                <div class="header-logo">
                    <img src="img/logo5.png" >
                </div>
            </div>
            <div class="span8">
                <div class="header-title">
                    <p class="main-title">ระบบสารสนเทศฝึกประสบการณ์วิชาชีพ</p>
                    <p class="sub-title">สาขาวิทยาการคอมพิวเตอร์และเทคโนโลยีสารสนเทศ</p>
                </div>
            </div>
        </div>
    </div>
</div>
<hr style="border-top: 1px solid #2b4d71;border-bottom: 1px solid #2b4d71;"> 
<div class="navigation">
    <div class="navigation-layout">
        <ul >
            <li><a href="#">หน้าหลัก</a></li>
            <li><a href="#">เว็บบอร์ด</a></li>
            <li><a href="#">เกี่ยวกับเรา</a></li>   
            <li><a href="#">ติดต่อเรา</a></li>   
        </ul>
    </div>
</div>